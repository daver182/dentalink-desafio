###Modelo de base de datos


Volcado del archivo **modelo.sql**

	SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
	SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
	SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

	-- -----------------------------------------------------
	-- Schema cursera
	-- -----------------------------------------------------
	CREATE SCHEMA IF NOT EXISTS `cursera` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
	USE `cursera` ;
	
	-- -----------------------------------------------------
	-- Table `cursera`.`curso`
	-- -----------------------------------------------------
	CREATE TABLE IF NOT EXISTS `cursera`.`curso` (
	 `id` INT NOT NULL AUTO_INCREMENT,
	 `nombre` VARCHAR(45) NULL,
	 PRIMARY KEY (`id`))
	ENGINE = InnoDB;
	
	
	-- -----------------------------------------------------
	-- Table `cursera`.`academico`
	-- -----------------------------------------------------
	CREATE TABLE IF NOT EXISTS `cursera`.`academico` (
	 `id` INT NOT NULL AUTO_INCREMENT,
	 `nombre` VARCHAR(45) NULL,
	 PRIMARY KEY (`id`))
	ENGINE = InnoDB;
	
	
	-- -----------------------------------------------------
	-- Table `cursera`.`alumno`
	-- -----------------------------------------------------
	CREATE TABLE IF NOT EXISTS `cursera`.`alumno` (
	 `id` INT NOT NULL AUTO_INCREMENT,
	 `rut` VARCHAR(45) NULL,
	 `nombre` VARCHAR(45) NULL,
	 PRIMARY KEY (`id`))
	ENGINE = InnoDB;
	
	
	-- -----------------------------------------------------
	-- Table `cursera`.`curso_academico`
	-- -----------------------------------------------------
	CREATE TABLE IF NOT EXISTS `cursera`.`curso_academico` (
	 `id` INT NOT NULL AUTO_INCREMENT,
	 `academico_id` INT NOT NULL,
	 `curso_id` INT NOT NULL,
	 PRIMARY KEY (`id`),
	 INDEX `fk_curso_academico_academico_idx` (`academico_id` ASC),
	 INDEX `fk_curso_academico_curso1_idx` (`curso_id` ASC),
	 CONSTRAINT `fk_curso_academico_academico`
	   FOREIGN KEY (`academico_id`)
	   REFERENCES `cursera`.`academico` (`id`)
	   ON DELETE NO ACTION
	   ON UPDATE NO ACTION,
	 CONSTRAINT `fk_curso_academico_curso1`
	   FOREIGN KEY (`curso_id`)
	   REFERENCES `cursera`.`curso` (`id`)
	   ON DELETE NO ACTION
	   ON UPDATE NO ACTION)
	ENGINE = InnoDB;
	
	
	-- -----------------------------------------------------
	-- Table `cursera`.`curso_alumno`
	-- -----------------------------------------------------
	CREATE TABLE IF NOT EXISTS `cursera`.`curso_alumno` (
	 `id` INT NOT NULL AUTO_INCREMENT,
	 `curso_id` INT NOT NULL,
	 `alumno_id` INT NOT NULL,
	 PRIMARY KEY (`id`),
	 INDEX `fk_curso_alumno_curso1_idx` (`curso_id` ASC),
	 INDEX `fk_curso_alumno_alumno1_idx` (`alumno_id` ASC),
	 CONSTRAINT `fk_curso_alumno_curso1`
	   FOREIGN KEY (`curso_id`)
	   REFERENCES `cursera`.`curso` (`id`)
	   ON DELETE NO ACTION
	   ON UPDATE NO ACTION,
	 CONSTRAINT `fk_curso_alumno_alumno1`
	   FOREIGN KEY (`alumno_id`)
	   REFERENCES `cursera`.`alumno` (`id`)
	   ON DELETE NO ACTION
	   ON UPDATE NO ACTION)
	ENGINE = InnoDB;
	
	
	-- -----------------------------------------------------
	-- Table `cursera`.`prueba`
	-- -----------------------------------------------------
	CREATE TABLE IF NOT EXISTS `cursera`.`prueba` (
	 `id` INT NOT NULL AUTO_INCREMENT,
	 `nombre` VARCHAR(45) NULL,
	 `curso_id` INT NOT NULL,
	 PRIMARY KEY (`id`),
	 INDEX `fk_curso_prueba_curso1_idx` (`curso_id` ASC),
	 CONSTRAINT `fk_curso_prueba_curso1`
	   FOREIGN KEY (`curso_id`)
	   REFERENCES `cursera`.`curso` (`id`)
	   ON DELETE NO ACTION
	   ON UPDATE NO ACTION)
	ENGINE = InnoDB;
	
	
	-- -----------------------------------------------------
	-- Table `cursera`.`prueba`
	-- -----------------------------------------------------
	CREATE TABLE IF NOT EXISTS `cursera`.`prueba` (
	 `id` INT NOT NULL AUTO_INCREMENT,
	 `nombre` VARCHAR(45) NULL,
	 `curso_id` INT NOT NULL,
	 PRIMARY KEY (`id`),
	 INDEX `fk_curso_prueba_curso1_idx` (`curso_id` ASC),
	 CONSTRAINT `fk_curso_prueba_curso1`
	   FOREIGN KEY (`curso_id`)
	   REFERENCES `cursera`.`curso` (`id`)
	   ON DELETE NO ACTION
	   ON UPDATE NO ACTION)
	ENGINE = InnoDB;
	
	
	-- -----------------------------------------------------
	-- Table `cursera`.`alumno_prueba`
	-- -----------------------------------------------------
	CREATE TABLE IF NOT EXISTS `cursera`.`alumno_prueba` (
	 `id` INT NOT NULL AUTO_INCREMENT,
	 `alumno_id` INT NOT NULL,
	 `prueba_id` INT NOT NULL,
	 `nota` FLOAT NOT NULL,
	 PRIMARY KEY (`id`),
	 INDEX `fk_alumno_prueba_alumno1_idx` (`alumno_id` ASC),
	 INDEX `fk_alumno_prueba_curso_prueba1_idx` (`prueba_id` ASC),
	 CONSTRAINT `fk_alumno_prueba_alumno1`
	   FOREIGN KEY (`alumno_id`)
	   REFERENCES `cursera`.`alumno` (`id`)
	   ON DELETE NO ACTION
	   ON UPDATE NO ACTION,
	 CONSTRAINT `fk_alumno_prueba_curso_prueba1`
	   FOREIGN KEY (`prueba_id`)
	   REFERENCES `cursera`.`prueba` (`id`)
	   ON DELETE NO ACTION
	   ON UPDATE NO ACTION)
	ENGINE = InnoDB;
	
	
	SET SQL_MODE=@OLD_SQL_MODE;
	SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
	SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
	
	-- -----------------------------------------------------
	-- Data for table `cursera`.`curso`
	-- -----------------------------------------------------
	START TRANSACTION;
	USE `cursera`;
	INSERT INTO `cursera`.`curso` (`id`, `nombre`) VALUES (NULL, 'programación');
	INSERT INTO `cursera`.`curso` (`id`, `nombre`) VALUES (NULL, 'redes');
	
	COMMIT;
	
	
	-- -----------------------------------------------------
	-- Data for table `cursera`.`academico`
	-- -----------------------------------------------------
	START TRANSACTION;
	USE `cursera`;
	INSERT INTO `cursera`.`academico` (`id`, `nombre`) VALUES (NULL, 'Academico 1');
	INSERT INTO `cursera`.`academico` (`id`, `nombre`) VALUES (NULL, 'Academico 2');
	
	COMMIT;
	
	
	-- -----------------------------------------------------
	-- Data for table `cursera`.`alumno`
	-- -----------------------------------------------------
	START TRANSACTION;
	USE `cursera`;
	INSERT INTO `cursera`.`alumno` (`id`, `rut`, `nombre`) VALUES (NULL, '13452324', 'Daniel');
	INSERT INTO `cursera`.`alumno` (`id`, `rut`, `nombre`) VALUES (NULL, '2435345', 'Andres');
	
	COMMIT;
	
	
	-- -----------------------------------------------------
	-- Data for table `cursera`.`curso_academico`
	-- -----------------------------------------------------
	START TRANSACTION;
	USE `cursera`;
	INSERT INTO `cursera`.`curso_academico` (`id`, `academico_id`, `curso_id`) VALUES (NULL, 1, 1);
	INSERT INTO `cursera`.`curso_academico` (`id`, `academico_id`, `curso_id`) VALUES (NULL, 2, 2);
	
	COMMIT;
	
	
	-- -----------------------------------------------------
	-- Data for table `cursera`.`curso_alumno`
	-- -----------------------------------------------------
	START TRANSACTION;
	USE `cursera`;
	INSERT INTO `cursera`.`curso_alumno` (`id`, `curso_id`, `alumno_id`) VALUES (NULL, 1, 1);
	INSERT INTO `cursera`.`curso_alumno` (`id`, `curso_id`, `alumno_id`) VALUES (NULL, 2, 1);
	INSERT INTO `cursera`.`curso_alumno` (`id`, `curso_id`, `alumno_id`) VALUES (NULL, 1, 2);
	INSERT INTO `cursera`.`curso_alumno` (`id`, `curso_id`, `alumno_id`) VALUES (NULL, 2, 2);
	
	COMMIT;
	
	
	-- -----------------------------------------------------
	-- Data for table `cursera`.`prueba`
	-- -----------------------------------------------------
	START TRANSACTION;
	USE `cursera`;
	INSERT INTO `cursera`.`prueba` (`id`, `nombre`) VALUES (NULL, 'prueba programacion 1');
	INSERT INTO `cursera`.`prueba` (`id`, `nombre`) VALUES (NULL, 'prueba programacion 2');
	INSERT INTO `cursera`.`prueba` (`id`, `nombre`) VALUES (NULL, 'prueba programacion 3');
	INSERT INTO `cursera`.`prueba` (`id`, `nombre`) VALUES (NULL, 'prueba redes 1');
	INSERT INTO `cursera`.`prueba` (`id`, `nombre`) VALUES (NULL, 'prueba redes 2');
	INSERT INTO `cursera`.`prueba` (`id`, `nombre`) VALUES (NULL, 'prueba redes 3');
	
	COMMIT;
	
	
	-- -----------------------------------------------------
	-- Data for table `cursera`.`prueba`
	-- -----------------------------------------------------
	START TRANSACTION;
	USE `cursera`;
	INSERT INTO `cursera`.`prueba` (`id`, `nombre`, `curso_id`) VALUES (NULL, 'prueba programacion 1', 1);
	INSERT INTO `cursera`.`prueba` (`id`, `nombre`, `curso_id`) VALUES (NULL, 'prueba programacion 2', 1);
	INSERT INTO `cursera`.`prueba` (`id`, `nombre`, `curso_id`) VALUES (NULL, 'prueba programacion 3', 1);
	INSERT INTO `cursera`.`prueba` (`id`, `nombre`, `curso_id`) VALUES (NULL, 'prueba redes 1', 2);
	INSERT INTO `cursera`.`prueba` (`id`, `nombre`, `curso_id`) VALUES (NULL, 'prueba redes 2', 2);
	INSERT INTO `cursera`.`prueba` (`id`, `nombre`, `curso_id`) VALUES (NULL, 'prueba redes 3', 2);
	
	COMMIT;
	
	
	-- -----------------------------------------------------
	-- Data for table `cursera`.`alumno_prueba`
	-- -----------------------------------------------------
	START TRANSACTION;
	USE `cursera`;
	INSERT INTO `cursera`.`alumno_prueba` (`id`, `alumno_id`, `prueba_id`, `nota`) VALUES (NULL, 1, 1, 5.5);
	INSERT INTO `cursera`.`alumno_prueba` (`id`, `alumno_id`, `prueba_id`, `nota`) VALUES (NULL, 1, 2, 6.3);
	INSERT INTO `cursera`.`alumno_prueba` (`id`, `alumno_id`, `prueba_id`, `nota`) VALUES (NULL, 1, 3, 6.8);
	INSERT INTO `cursera`.`alumno_prueba` (`id`, `alumno_id`, `prueba_id`, `nota`) VALUES (NULL, 2, 1, 4.5);
	INSERT INTO `cursera`.`alumno_prueba` (`id`, `alumno_id`, `prueba_id`, `nota`) VALUES (NULL, 2, 2, 5.5);
	INSERT INTO `cursera`.`alumno_prueba` (`id`, `alumno_id`, `prueba_id`, `nota`) VALUES (NULL, 2, 3, 6);
	INSERT INTO `cursera`.`alumno_prueba` (`id`, `alumno_id`, `prueba_id`, `nota`) VALUES (NULL, 1, 4, 5.8);
	INSERT INTO `cursera`.`alumno_prueba` (`id`, `alumno_id`, `prueba_id`, `nota`) VALUES (NULL, 1, 5, 5.6);
	INSERT INTO `cursera`.`alumno_prueba` (`id`, `alumno_id`, `prueba_id`, `nota`) VALUES (NULL, 1, 6, 5.7);
	INSERT INTO `cursera`.`alumno_prueba` (`id`, `alumno_id`, `prueba_id`, `nota`) VALUES (NULL, 2, 4, 3);
	INSERT INTO `cursera`.`alumno_prueba` (`id`, `alumno_id`, `prueba_id`, `nota`) VALUES (NULL, 2, 5, 3.5);
	INSERT INTO `cursera`.`alumno_prueba` (`id`, `alumno_id`, `prueba_id`, `nota`) VALUES (NULL, 2, 6, 4);
	
	COMMIT;

