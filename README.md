#Desafío Dentalink

###Programación

*1.­ Escriba una función/método que determine la cantidad de 0’s a la derecha de n! (factorial)*

La función se dedujo apoyándose en un ejercicio similar, el cual se describe en este  [link](https://mchouza.wordpress.com/2009/06/23/cantidad-de-ceros-al-final-de-un-factorial/). Ahí se describe la metodología para obtener la cantidad de ceros a la derecha de un factorial. Primero se define que:

	n = m * 10^k
	
Basándose en esto el autor propone que para averiguar la cantidad de ceros solo es necesario saber por cuantos números es divisible *m*, y ademas se agrega que si un número es divisible por 2 y por 5 es divisible por 10.

Lo único que queda es generar una función recursiva que reciba el valor de n y lo someta a una evaluación continua, dividiendo el valor por 5 y tomando solo su parte entera, la cual se vuelve a ingresar en la misma función. El resultado de cada llamado a la función se va sumando hasta que el valor de *n* es inferior a 0, ahí se termina la evaluación y se obtiene la cantidad de ceros a la derecha de un factorial.

Definición de la función utilizando Javascript:

	function factorial(n){
		if(n > 0){
			return Math.floor(n / 5) + factorial(Math.floor(n / 5));
		} else {
			return 0;
		}
	}
	
La función devuelve la cantidad de ceros al ingresar un valor de *n*, de la siguiente manera:

	factorial(Math.pow(10, 1)) >>> 2
	factorial(5) >>> 1
	factorial(Math.pow(10, 4)) >>> 2499
	
###Modelo de datos

*1.- Modelo de base datos*

El problema se modeló utilizando los elementos propuestos en el enunciado. Un curso puede tener muchas pruebas, y muchos alumnos, y los alumnos pueden pertenecer a muchos cursos y tener muchas pruebas de esos cursos.
Se agregaron algunos datos solo para obtener resultados en las consultas. No se hizo un mayor trabajo para optimizar la base de datos.

El [Modelo](modelo.md) fue generado utilizando MySQLWorkbench.

![image](modelo.png)

*2.- Escriba un Query que entregue la lista de alumnos para el curso “programación”.*

	SELECT alumno.* FROM curso	
	JOIN curso_alumno ON curso_alumno.curso_id = curso.id
	JOIN alumno ON alumno.id = curso_alumno.alumno_id
	WHERE curso.nombre = 'programación'
	
*3.- ­Escriba un Query que calcule el promedio de notas de un alumno en un curso.*
	
	SELECT ROUND(AVG(AP.nota), 1) AS Promedio FROM curso C
	JOIN curso_alumno CA ON CA.curso_id = C.id
	JOIN alumno A ON A.id = CA.alumno_id
	JOIN alumno_prueba AP ON AP.alumno_id = A.id
	JOIN prueba P ON C.id = P.curso_id
	
	WHERE C.nombre = 'programación'
	AND P.id = AP.prueba_id
	AND A.nombre = 'Daniel'
	
*4.- Escriba un Query que entregue a los alumnos y el promedio que tiene en cada ramo.*	
		
	SELECT A.nombre AS Alumno, ROUND(AVG(AP.nota), 1) AS Promedio, C.nombre AS 	Curso FROM curso C
	JOIN curso_alumno CA ON CA.curso_id = C.id
	JOIN alumno A ON A.id = CA.alumno_id
	JOIN alumno_prueba AP ON AP.alumno_id = A.id
	JOIN prueba P ON C.id = P.curso_id

	WHERE P.id = AP.prueba_id
	GROUP BY C.id, A.id
	ORDER BY A.nombre
	
*5.- Escriba un Query que lista a todos los alumnos con más de un ramo con promedio rojo.*

	SELECT promedio.Alumno FROM 
	(SELECT A.id AS id, A.nombre AS Alumno, ROUND(AVG(AP.nota), 1) AS Nota, C.nombre AS Curso FROM curso C
		JOIN curso_alumno CA ON CA.curso_id = C.id
		JOIN alumno A ON A.id = CA.alumno_id
		JOIN alumno_prueba AP ON AP.alumno_id = A.id
		JOIN prueba P ON C.id = P.curso_id
		WHERE P.id = AP.prueba_id
		GROUP BY C.id, A.id) promedio
	GROUP BY promedio.id
	HAVING  SUM(IF( promedio.nota < 4,1,0)) > 1
	
*6.- Se tiene una tabla con información de jugadores de tenis: PLAYERS(Nombre, Pais, Ranking)* 

El resultado es imposible saberlo ya que se compara cada fila con la siguiente, por lo tanto depende del valor de la columna ranking. En el caso de que los valores fueran ascendentes el resultado sería 190. En otro caso si los valores fueran todos iguales la consulta no devolvería ningun valor. En el enunciado no se indica en ninguna parte que los valores de la columna Ranking fueran ascendetes o descendentes, pueden valores aleatorios, lo que hace mas dificil saber el resultado de esa consulta.
